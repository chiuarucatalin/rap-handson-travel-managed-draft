## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Mon May 24 2021 12:15:18 GMT+0300 (Ora de vară a Europei de Est)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.1.11|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>List Report Object Page V2|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>https://vhosts4d.awscloud.msg.de:8443/sap/opu/odata/sap/ZRAPH_UI_TRAVELWDTP_V2/
|**Module Name**<br>travelwdtp|
|**Application Title**<br>Travel Management|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|
|**Main Entity**<br>Travel|
|**Navigation Entity**<br>to_Booking|

## travelwdtp

Travel Management - Scenario Managed/Draft

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


